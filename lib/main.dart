import 'package:flutter/material.dart';
import 'package:flutter_practice_6/pageData.dart';

import 'package:intl/intl.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: HomePage(),
    );
  }
}

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  PageData data = PageData(
      pageDate: DateTime.now(),
      pageTime: TimeOfDay.now(),
      pageSliderValue: 0.0,
      pageGenderValue: 'Male');

  DateTime pickedDate = DateTime.now();
  TimeOfDay pickedTime = TimeOfDay.now();
  double sliderValue = 0.0;
  String pickedGender = 'Male';

  void _chooseDate() {
    showDatePicker(
      context: context,
      initialDate: data.pageDate,
      firstDate: DateTime(1990),
      lastDate: DateTime(2050),
    ).then((value) {
      if (value == null) {
        return;
      }
      setState(() {
        data.pageDate = value;
      });
    });
  }

  void _chooseTime() {
    showTimePicker(
      context: context,
      initialTime: TimeOfDay.now(),
    ).then((value) {
      if (value == null) {
        return;
      }
      setState(() {
        data.pageTime = value;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    Size deviceSize = MediaQuery.of(context).size;

    return Scaffold(
      appBar: AppBar(
        title: Text('Flutter practice 6'),
      ),
      body: SizedBox(
        height: deviceSize.height - AppBar().preferredSize.height,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            Container(
              alignment: Alignment.center,
              child: InkWell(
                onTap: _chooseDate,
                child:
                    Text('Pick Date: ${DateFormat.yMd().format(data.pageDate)}'),
              ),
            ),
            Container(
              alignment: Alignment.center,
              child: InkWell(
                onTap: _chooseTime,
                child: Text('Pick Date: ${data.pageTime.format(context)}'),
              ),
            ),
            Container(
              alignment: Alignment.center,
              child: Column(
                children: [
                  Text(
                    data.pageSliderValue.toStringAsFixed(2),
                  ),
                  Slider(
                    value: data.pageSliderValue,
                    min: 0,
                    max: 1,
                    onChanged: (value) {
                      setState(
                        () {
                          data.pageSliderValue = value;
                        },
                      );
                    },
                  ),
                ],
              ),
            ),
            Container(
              alignment: Alignment.center,
              child: DropdownButton(
                value: data.pageGenderValue,
                items: <String>['Male', 'Female', 'Other'].map((value) {
                  return DropdownMenuItem(
                    value: value,
                    child: Text(value),
                  );
                }).toList(),
                onChanged: (value) {
                  setState(() {
                    data.pageGenderValue = value;
                  });
                },
              ),
            ),
            ElevatedButton(
              onPressed: () => data.toString(),
              child: Text('Button'),
            ),
          ],
        ),
      ),
    );
  }
}
