


import 'package:flutter/material.dart';

class PageData {
  DateTime pageDate;
  TimeOfDay pageTime;
  double pageSliderValue;
  String pageGenderValue;

  PageData({
      this.pageDate,
      this.pageTime,
      this.pageSliderValue,
      this.pageGenderValue});



  @override
  String toString() {
    print('pageDate: ${this.pageDate}');
    print('pageTime: ${this.pageTime}');
    print('pageSliderValue: ${this.pageSliderValue}');
    print('pageGenderValue: ${this.pageGenderValue}');
    return super.toString();
  }

}